from bintrees.avltree import AVLTree
import re

class LexicalAnalyzerFiniteAutomata:
    _PARSE_LINE = '([0-9]*\.[0-9]*|#?[a-zA-Z]*)[\w]+|".*"|(::)+|(>>)+|(<<)+|[<,>,\-,\+,:,*,=,;,::]|\(|\)|{|}'
    _ID = "ID"
    _CONST = "CONST"
    _FIP = "../FIP"
    _TS = "../TS"
    _TC = "../TC"

    def __init__(self, file_name,codes,verify_identifier= None,verify_constant=None):
        self.__file_name = file_name
        self.__codes = codes
        self.__ts =AVLTree()
        self.__tc = AVLTree()
        self.__result = []
        self.__verify_identifier = verify_identifier
        self.__verify_constant = verify_constant

    def _orderTs(self):
        k= 1
        def x(ke,v):
            nonlocal k
            self.__ts.insert(ke,k)
            k+=1
        self.__ts.foreach(x)
        
    def _orderTc(self):
        k = 1;
        def x(ke,v):
            nonlocal k
            self.__tc.insert(ke,k)
            k+=1
        self.__tc.foreach(x)
    
    def _parse_line(self,line):#generator over groups
        tokens = re.finditer(self._PARSE_LINE, line) 
        for token in tokens:
            yield token.group(0)
    
    def _is_code(self,key):
        try:
            self.__codes[key]
            return True
        except Exception:
            return False
        
    def _is_identifier(self,value):
        return self.__verify_identifier.verify(value)
    
    def _is_constant(self,value):
        return self.__verify_constant.verify(value)
                
    def _check_token(self,token):
        if token != None:
            if self._is_code(token):
                self.__result.append({self.__codes[token] : '/'})
                return
            if self._is_identifier(token):
                ids = self.__ts.get(token, None)
                if ids == None:
                    self.__ts.insert(token, ++self.__ts.count)
                self.__result.append({self._ID:token})
                return
            if self._is_constant(token):
                const = self.__tc.get(token,None)
                if const == None:
                    self.__tc.insert(token, ++self.__tc.count)
                self.__result.append({self._CONST:token})
                return 
            raise RuntimeError(token)
        
    def _analyze(self):
        fr = open(self.__file_name,"r")
        lineNumber = 0
        with fr:
            for line in fr:
                line = line.strip()
                try:
                    generator = self._parse_line(line)
                    token = ''
                    while token != None:
                        result = None
                        token = next(generator,None)
                        token1 = next(generator,None)
                        if token !=None and token1 !=None:
                            result = token+" "+token1
                        if self._is_code(result):
                            self.__result.append({self.__codes[result]:'/'})
                        else:
                            self._check_token(token)
                            self._check_token(token1) 
                    lineNumber+=1                   
                except RuntimeError as err:
                    raise RuntimeError("error at line " + str(lineNumber)+" "+str(err))
                    
    def run(self):
        self._analyze()
        fw = open(self._FIP,"w")
        self._orderTs()
        self._orderTc()
        with fw:
            for d in self.__result:
                for k in d.keys():
                    key = k
                    value = d[key]
                    if key == self._ID:
                        value = self.__ts.get_value(d[key])
                        key = self.__codes[key]
                    if key == self._CONST:
                        value = self.__tc.get_value(d[key])
                        key = self.__codes[key]
                    fw.write(str(key)+" "+str(value)+'\n')
        fw = open(self._TS,"w")
        with fw:
            self.__ts.foreach(lambda x,y : fw.write(str(x)+' '+str(y)+'\n'))
        fw = open(self._TC,"w")
        with fw:
            self.__tc.foreach(lambda x,y : fw.write(str(x)+' '+str(y)+'\n'))