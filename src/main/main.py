from lexical_analyzer.lexical_analyzer import LexicalAnalyzer
from finite_automata.finite_automata import FiniteAutomata
from lexical_analyzer.lexical_analyzer_finite_automata import LexicalAnalyzerFiniteAutomata
def __read_from_file(file_name,spl):
    file = open(file_name,"r")
    with file:
        for line in file:
            line = line.strip()
            line = line.split(spl)
            yield line

if __name__ == '__main__':
    codes = {}
    for k,v in __read_from_file("../codes.txt", "  "):
        codes[k] = v
#     print(codes)
#     l = LexicalAnalyzer("../program.c",codes)
#     l.run()
    id_fa = FiniteAutomata("../identifier_finite_automata.af")
    const_fa = FiniteAutomata("../constant_finite_automata.af")
    l = LexicalAnalyzerFiniteAutomata("../program.cpp",codes,verify_identifier=id_fa,verify_constant=const_fa)
    l.run()
    print("Hello world")