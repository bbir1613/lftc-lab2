class FiniteAutomata(object):
    def __init__(self, file_name):
        self.__alphabet = set()
        self.__states = set()
        self.__final_states = set()
        self.__tranzitions = []
        self.__initial_state = None
        self.__delta = {}
        self._load_file(file_name)
    
    def _load_file(self,file_name):
        DELIMITER = ','
        TRANZITIONS_DELIMITER = ';'
        STATE_DELIMITER = '-'
        
        def __add_tranzition(tranzition):
            initial_tranzition = tranzition.pop(0)
            final_tranzition = tranzition.pop(0)
            for word in tranzition:
                self.__tranzitions.append(initial_tranzition+DELIMITER+word+DELIMITER+final_tranzition)
                
        def __add_state(state,DELIMITER):
            if DELIMITER not in state:
                self.__final_states.add(state)
            else:
                state = state.split(DELIMITER)
                for s in state:
                    self.__final_states.add(s)

        file = open(file_name,'r')        
        with file:
            for state in file.readline().strip().split(DELIMITER):
                self.__states.add(state)
            for word in file.readline().strip().split(DELIMITER):
                self.__alphabet.add(word)
            #read initial state
            tranzition = file.readline().strip()[:-1].split(DELIMITER)
            self.__initial_state = tranzition[0]
            __add_tranzition(tranzition)
            
            for token in file:
                token = token.strip()
                if TRANZITIONS_DELIMITER in token:
                    token = token[:-1]
                    tranzition = token.split(DELIMITER)
                    __add_tranzition(tranzition)
                elif STATE_DELIMITER in token:
                    token = token[:-1]
                    __add_state(token,DELIMITER)
            for tranzition in self.__tranzitions:
                tokens = tranzition.split(DELIMITER)
                key = tokens[0] +DELIMITER+tokens[1]
                if key not in self.__delta.keys():
                    self.__delta[key] = []
                self.__delta[key].append(tokens[2])
#             print(self.__delta)
#             print(self.__final_states)
#             print(self.__initial_state)

    def _verify_sequence(self,sequence,state,delta):
        try:
            if len(sequence) == 1:
                for state in delta[state+","+sequence[0]]:
                    if state in self.__final_states:
                        return True
                return False
            for next_state in delta[state+","+sequence[0]]:
                if self._verify_sequence(sequence[1:], next_state, delta) == True:
                    return True
            return False
        except Exception as _:
#             print('Exception here ' + str(ex))
            return False
        
    def verify(self,sequence):
        return self._verify_sequence(sequence, self.__initial_state, self.__delta)
    
    def verify_longest_sequence(self,sequence):
        while self.verify(sequence) == False and len(sequence) > 0:
            sequence = sequence[:-1]
        return sequence
    